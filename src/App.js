import "./App.css";
import React from "react";
import ProductList from "./components/ProductList/ProductList";
import Header from "./components/Header/Header";
import ModalData from "./components/ModalData/ModalData";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: [],
      cartData: [],
      showAddToCartModal:false,
      favorites: [],
      showAddToFavoriteModal:false,
      checkItem: '',
    };
  }

  componentDidMount() {
    fetch("data.json")
      .then((res) => res.json())
      .then((json) => this.setState({ product: json }));

    if (localStorage.getItem("cartData")) {
        this.setState({ cartData: JSON.parse(localStorage.cartData) });
    }

    if (localStorage.getItem("favorites")) {
      this.setState({ favorites: JSON.parse(localStorage.favorites) });
    }
  }

  showAddToCartModal = () => {
    this.setState({ showAddToCartModal: !this.state.showAddToCartModal });
  }

  showAddToFavoriteModal = () => {
    this.setState({ showAddToFavoriteModal: !this.state.showAddToFavoriteModal });
  }

  checkItem = (product) => {
    this.setState({ checkItem: product })
  }

  addToCart = (product) => {
    let isInArr = false;
    this.state.cartData.forEach(el => {
      if (product.id === el.id) {
        isInArr = true
      }
    })
    if (!isInArr) {
      this.setState({ cartData: [...this.state.cartData, product] })
      localStorage.cartData = JSON.stringify([...this.state.cartData, product]);

    }
  };

  addToFavorite = (product) => {
    let isInFav = false;
    this.state.favorites.forEach(el => {
      if (product.id === el.id) {
         isInFav = true
      }
    })

    if (!isInFav) {
      this.setState({ favorites: [...this.state.favorites, product] })
      localStorage.favorites = JSON.stringify([...this.state.favorites, product]);
    }
  };

  deleteFavorite = (product) => {
    const clearData = this.state.favorites.filter((item) =>   item.id !== product.id)
    this.setState({ favorites: clearData })
    localStorage.favorites = JSON.stringify(clearData);
  }

  render() {
    return (
      <div className="App">
        <Header favorites={this.state.favorites} cartData={this.state.cartData} />
        <ProductList
          checkItem={this.checkItem}
          showAddToCartModal={this.showAddToCartModal}
          favorites={this.state.favorites}
          showAddToFavoriteModal={this.showAddToFavoriteModal}
          deleteFavorite={this.deleteFavorite}
          cartData={this.state.cartData}
          products={this.state.product}
        />
         {this.state.showAddToCartModal && (
            <ModalData
              modalHeader="Do you want to add?"
              modalTitle="Cart Modal"
              modalText="Happy schopping!!!"
              onClick={this.showAddToCartModal}
              addData={this.addToCart}
              checkItem={this.state.checkItem}
              background="blue"
              colorButtonOk="orange"
              colorButtonCancel="red"
            />
         )}
        {this.state.showAddToFavoriteModal && (
            <ModalData
                modalHeader="Do you want to add favorite?"
                modalTitle="Favorite Modal"
                modalText="Happy schopping!!!"
                onClick={this.showAddToFavoriteModal}
                addData={this.addToFavorite}
                checkItem={this.state.checkItem}
                background="white"
                colorButtonOk="orange"
                colorButtonCancel="red"
            />
        )}
      </div>
    );
  }
}

export default App;
