import React from "react";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import "./Header.scss";
import { BsStarFill, BsFillCartFill } from "react-icons/bs";
import PropTypes from "prop-types";

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Navbar bg="primary" variant="dark">
        <Container>
          <Navbar.Brand href="#home">
            <img
              className="logo1"
              src={process.env.PUBLIC_URL + "/weblogo1.png"}
            />
          </Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#features">Favorites</Nav.Link>
            <Nav.Link href="#pricing">Shopping</Nav.Link>
            <Nav.Link href="#favorite">
              <BsStarFill />
              <span>{this.props.favorites.length}</span>
            </Nav.Link>
            <Nav.Link href="#cart">
              <BsFillCartFill />
              <span>{this.props.cartData.length}</span>
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    );
  }
}

export default Header;

Header.propTypes = {
  favorites: PropTypes.array.isRequired,
  cartData: PropTypes.array.isRequired,
};
