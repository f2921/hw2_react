import React from "react";
import "./ModalData.scss";
import PropTypes from "prop-types";

export default class ModalData extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="modal-background" onClick={this.props.onClick}>
        <div
          className="modalData"
          style={{ backgroundColor: this.props.background }}
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <div className="modal-header">
            <span className="close" onClick={this.props.onClick}>
              &times;
            </span>
            <h2 className="modalHeader">{this.props.modalHeader}</h2>
          </div>
          <div className="modal__content-container">
            <h3>{this.props.modalTitle}</h3>
            <p>{this.props.modalText}</p>
            {/*<p>{this.props.productData.title}</p>*/}
            <div className="button-block">
              <button
                onClick={() => {
                  this.props.onClick();
                  this.props.addData(this.props.checkItem);
                }}
                className="modal__btn"
              >
                Add
              </button>
              <button onClick={this.props.onClick} className="modal__btn">
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ModalData.defaultProps = {
  background: "rgb(69, 188, 236)",
  modalText: "Are you sure you want to delete it?",
};

ModalData.propTypes = {
  modalHeader: PropTypes.string,
  onClick: PropTypes.func,
  modalTitle: PropTypes.string,
  modalText: PropTypes.string,
  checkItem: PropTypes.func,
  addData: PropTypes.func,
}
