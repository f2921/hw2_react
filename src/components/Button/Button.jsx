import React from "react";
import "./styles.scss";
import PropTypes from 'prop-types';

export default class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button
        onClick={() => {
          this.props.onClick();
        }}
        className={this.props.className ? this.props.className : "button"}
        style={{ backgroundColor: this.props.background }}
      >
        {this.props.text}
      </button>
    );
  }
}
Button.PropTypes={
  background:PropTypes.string,
  onClick:PropTypes.func,
  text:PropTypes.string,
}