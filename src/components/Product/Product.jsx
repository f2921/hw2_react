import React from "react";
import "./Product.scss";
import { BsStarFill } from "react-icons/bs";
import PropTypes from "prop-types";

export default class Product extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props.data);
    const { title, price, id, image } = this.props.data;

    return (
      <div className="item">
        <button
          className={`star ${this.props.checkFav ? "active" : ""}`}
          onClick={() => {
            this.props.checkFav
              ? this.props.deleteFavorite(this.props.data)
              : this.props.showAddToFavoriteModal();
            this.props.checkItem(this.props.data);
          }}
        >
          <BsStarFill />
        </button>
        <div className="item__content">
          <div className="item__content--img-block">
            <img src={image} />
          </div>
          <div className="item__content--info-block">
            <h3 className="item-title">{title}</h3>
            <p className="item-price">{price} грн</p>
            <p>Артикул: {id}</p>
          </div>
        </div>
        <button
          onClick={() => {
            this.props.showAddToCartModal();
            this.props.checkItem(this.props.data);
          }}
          className="cartBtn"
        >
          Add to cart
        </button>
      </div>
    );
  }
}

Product.propTypes = {
  data: PropTypes.object,
  checkFav: PropTypes.func,
  deleteFavorite: PropTypes.func,
  showAddToFavoriteModal: PropTypes.func,
  showAddToCartModal: PropTypes.func,
  checkItem: PropTypes.func,
};
