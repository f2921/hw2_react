import React from "react";
import Product from "../Product/Product";
import "./ProductList.scss";
import PropTypes from "prop-types";

export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
  }

  checkFav = (data) => {
    return this.props.favorites.some((fav) => {
      return fav.id === data.id;
    });
  };

  render() {
    return (
      <div className="item-list">
        {this.props.products.map((product) => {
          return (
            <Product
              key={product.id}
              showAddToCartModal={this.props.showAddToCartModal}
              showAddToFavoriteModal={this.props.showAddToFavoriteModal}
              checkItem={this.props.checkItem}
              checkFav={this.checkFav(product)}
              deleteFavorite={this.props.deleteFavorite}
              data={product}
            />
          );
        })}
      </div>
    );
  }
}

ProductList.propTypes = {
  favorites: PropTypes.array,
  products: PropTypes.array,
  showAddToCartModal: PropTypes.func,
  showAddToFavoriteModal: PropTypes.func,
  deleteFavorite: PropTypes.func,
  checkItem: PropTypes.func,
};
